package br.unisinos.apollo.controller;

import br.unisinos.apollo.model.Gmail;
import br.unisinos.apollo.model.dao.ConnectionDAO;
import br.unisinos.apollo.model.dao.MailAccountDAO;
import java.sql.Connection;
import viso.mota.mail.model.bean.MailAccount;

/**
 * The controller class to Gmail methods.
 *
 * Created 09/09/2016
 *
 * @author Bruno Mota
 */
public class ExchangeController {

    /**
     * Gmail method to send an email.
     *
     * @param FROM who is sending the mail.
     * @param TO the email addresses to delivery this mail.
     * @param SUBJECT the message subject.
     * @param CONTENT the full content in HTML.
     * @param HASH the hash to validate the call.
     * @return Retrieve 1 if everything it's right.
     */
    public String sendMessage(final String FROM, final String TO,
            final String SUBJECT, final String HASH, final String CONTENT) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                MailAccountDAO mDAO = new MailAccountDAO();
                Connection connection = new ConnectionDAO().connect();
                Boolean exists = mDAO.validateAccountAddress(connection, FROM);
                if (exists) { //IF THE ADDRESS EXISTS
                    MailAccount ma = new MailAccountDAO().retrieveMailAccount(connection, FROM);
                    exists = mDAO.validateHashAddress(connection, FROM, HASH);
                    if (exists) { //IF THE ADDRESS AND HASH EXISTS
                        ma = new MailAccountDAO().retrieveMailAccount(connection, FROM, HASH);
                        try {
                            new Gmail().sendMessage(FROM, ma.getPassword(), TO, SUBJECT, CONTENT);
                            new MailAccountDAO().persistLog(connection, ma.getId(),
                                    "003 - Mensagem enviada de " + FROM + " para " + TO);
                        } catch (Exception e) {
                            new MailAccountDAO().persistLog(connection, ma.getId(),
                                    "004 - Falha interna ao enviar mensagem de "
                                    + FROM + " para " + TO + ". Mensagem: " + e.getMessage());
                        } finally {
                            new ConnectionDAO().disconnect(connection);
                        }
                    } else { //HASH HAS GONE WRONG
                        mDAO.persistLog(connection, ma.getId(),
                                "002 - Falha ao enviar mensagem de " + FROM
                                + " para " + TO + ". Mensangem: Hash inválido!");
                        new ConnectionDAO().disconnect(connection);
                    }
                } else { //MAIL ADDRESS HAS GONE WRONG
                    mDAO.persistLogNoUser(connection, "001 - Falha ao enviar mensagem de "
                            + FROM + " para " + TO + ". Mensagem: Endereço email inválido!");
                    new ConnectionDAO().disconnect(connection);
                }
            }
        }
        ).start();
        //return "1 - Parabêns vc chegou, aqui esta o que eu recebi: FROM: " + FROM + " TO: " + TO + " SUBJECT: " + SUBJECT + " HASH: " + HASH;
        return "1";
    }

    public String sendMessageAttach(final String FROM, final String TO, final String SUBJECT,
            final String HASH, final String CONTENT, final String FILEPATH) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                MailAccountDAO mDAO = new MailAccountDAO();
                Connection connection = new ConnectionDAO().connect();
                Boolean exists = mDAO.validateAccountAddress(connection, FROM);
                if (exists) { //IF THE ADDRESS EXISTS
                    MailAccount ma = new MailAccountDAO().retrieveMailAccount(connection, FROM);
                    exists = mDAO.validateHashAddress(connection, FROM, HASH);
                    if (exists) { //IF THE ADDRESS AND HASH EXISTS
                        ma = new MailAccountDAO().retrieveMailAccount(connection, FROM, HASH);
                        try {
                            new Gmail().sendMessageAttach(FROM, ma.getPassword(), TO, SUBJECT, CONTENT, FILEPATH);
                            new MailAccountDAO().persistLog(connection, ma.getId(),
                                    "003 - Mensagem enviada de " + FROM + " para " + TO);
                        } catch (Exception e) {
                            new MailAccountDAO().persistLog(connection, ma.getId(),
                                    "004 - Falha interna ao enviar mensagem de "
                                    + FROM + " para " + TO + ". Mensagem: " + e.getMessage());
                        } finally {
                            new ConnectionDAO().disconnect(connection);
                        }
                    } else { //HASH HAS GONE WRONG
                        mDAO.persistLog(connection, ma.getId(),
                                "002 - Falha ao enviar mensagem de " + FROM
                                + " para " + TO + ". Mensangem: Hash inválido!");
                        new ConnectionDAO().disconnect(connection);
                    }
                } else { //MAIL ADDRESS HAS GONE WRONG
                    mDAO.persistLogNoUser(connection, "001 - Falha ao enviar mensagem de "
                            + FROM + " para " + TO + ". Mensagem: Endereço email inválido!");
                    new ConnectionDAO().disconnect(connection);
                }
            }
        }
        ).start();
        //return "1 - Parabêns vc chegou, aqui esta o que eu recebi: FROM: " + FROM + " TO: " + TO + " SUBJECT: " + SUBJECT + " HASH: " + HASH;
        return "1";
    }
}
