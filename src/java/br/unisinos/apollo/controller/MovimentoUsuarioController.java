package br.unisinos.apollo.controller;

import br.unisinos.apollo.model.Gmail;
import br.unisinos.apollo.model.bean.MovimentoUsuario;
import br.unisinos.apollo.model.bean.User;
import br.unisinos.apollo.model.dao.ConnectionDAO;
import br.unisinos.apollo.model.dao.MovimentoUsuarioDAO;
import br.unisinos.apollo.model.dao.UserDAO;
import java.sql.Connection;
import java.util.List;

/**
 * Created 05/05/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class MovimentoUsuarioController {

    private final String USERMAIL = "apollo.unisinos@gmail.com";
    private final String USERPASSWORD = "Nov@2018";

    public void persistMonitoramentoUsuario(List<MovimentoUsuario> monitor) {
        Connection connection = new ConnectionDAO().connect();
        MovimentoUsuarioDAO muDAO = new MovimentoUsuarioDAO();
        for (int i = 0; i < monitor.size(); i++) {
            muDAO.movimentoUsuario(connection, monitor.get(i));
        }
        new ConnectionDAO().disconnect(connection);
    }

    public void doPredictionFeedback(Integer userID, Integer predictionID, Boolean feedback) {
        System.out.println("prediction feedback: " + feedback);
        Connection connection = new ConnectionDAO().connect();
        new MovimentoUsuarioDAO().setPositiveFeedback(connection, predictionID, feedback);
        User user = new UserDAO().getUserInfo(connection, userID);
        if (!feedback) {
            new Gmail().sendMessage(USERMAIL, USERPASSWORD, user.getMail(), "Apollo EFP - Emergência!",
                    "John pode ter sido vítima de uma queda. "
                    + "O serviço de emergência foi acionado automaticamente. "
                    + "Verifique imediatamente se teste precisa de ajuda.");
        }
        new ConnectionDAO().disconnect(connection);
    }

}
