package br.unisinos.apollo.controller;

import br.unisinos.apollo.model.bean.Extrinseco;
import br.unisinos.apollo.model.bean.Intrinseco;
import br.unisinos.apollo.model.bean.User;
import br.unisinos.apollo.model.dao.ConnectionDAO;
import br.unisinos.apollo.model.dao.ExtrinsecoDAO;
import br.unisinos.apollo.model.dao.IntrinsecoDAO;
import br.unisinos.apollo.model.dao.UserDAO;
import java.sql.Connection;
import java.util.Date;

/**
 * Created 07/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class UserController {

    public Double retrieveIMC(Integer id) {
        Connection connection = new ConnectionDAO().connect();
        User user = new UserDAO().getUserInfo(connection, id);
        //Calc the IMC
        Double imc = user.getWeight() / (user.getHeight() * user.getHeight());
        //Disconnect database
        new ConnectionDAO().disconnect(connection);
        //Return it
        return imc;
    }

    public Integer retrieveAge(Integer id) {
        Connection connection = new ConnectionDAO().connect();
        User user = new UserDAO().getUserInfo(connection, id);
        //Calc the age
        Date now = new Date();
        Integer age = now.getYear() - user.getBornDate().getYear();
        //Disconnet database
        new ConnectionDAO().disconnect(connection);
        //Return it
        return age;
    }

    public Integer getScore(Integer id) {
        Connection connection = new ConnectionDAO().connect();
        Intrinseco in = new IntrinsecoDAO().getIntrinseco(connection, id);
        Extrinseco ex = new ExtrinsecoDAO().getExtrinseco(connection, id);
        int score = 0;
        int posValue = 1;
        int negValue = 1;

        //IMC
        //IDADE
        //TEMPOREACAO
        if (in.getAntecedenteQueda()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getDeficitEquilibrio()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getDeficitVisual()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getDeficitMarcha()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getDemencia()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getMedoCair()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getFraquezaMuscular()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getDeficienciaAuditiva()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getDeficitPonderal()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getSensibilidade()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getDeficienciaFisica()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getOsteoporose()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getArtrite()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getDoencaReumatica()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getSincope()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getLesaoSistemaNervosoCentral()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getProblemasNoPe()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getDepressao()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getParkinson()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getVertigem()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getDiabetes()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getAlteracaoPostural()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getAntecedenteAVC()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getIncontinenciaUrinaria()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getComorbidade()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getAutoPercepcaoEstadoSaude()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (in.getDor()) {
            score += posValue;
        } else {
            score += negValue;
        }

        //===========================================================
        //======================= Extrinsico ========================
        //===========================================================
        if (ex.getCompromentimentoMobilidade()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getRiscoAmbiental()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getInternLongaPermanencia()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getViveSozinho()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getReducaoAtividadesBasicas()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getCapacidadeManterseEmPe()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getCapacidadeTransferirse()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getCapacidadeDeitarLevantar()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getDependenciaFuncional()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getMuitoAtivoFisicamente()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getSedentarismo()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getPolifarmacia()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getUsoDrogasPsicoativas()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getBenzoDiazepinicos()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getAntiPsicoticos()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getAntiDepressivos()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getSedativos()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getAnsioliticos()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getHipnoticos()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getFenotiazina()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getAntiHipertensivos()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getAntiEpileticos()) {
            score += posValue;
        } else {
            score += negValue;
        }

        if (ex.getConsumoAlcool()) {
            score += posValue;
        } else {
            score += negValue;
        }

        return score;
    }

}
