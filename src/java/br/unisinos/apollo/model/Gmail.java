package br.unisinos.apollo.model;

import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * The Gmail model class where the methods are really implemented.
 *
 * Created 09/09/2016 Updated 01/02/2017 - Now it's allowed to receive multiples
 * recipients.
 *
 * @author Bruno Mota
 */
public class Gmail {

    private final String SMTP_HOST = "smtp.gmail.com";
    private final String SMTP_PORT = "587";

    /**
     * Build the structure of adresses to send the mail. It's possible to
     * receive one simple mail, or more than one just adding the ; to separate
     * them.
     *
     * @param to the String containing the mail or mails.
     * @return the Address[] list.
     */
    private Address[] buildRecipients(String to) {
        Address[] addr = null;
        try {
            if (to.contains(";")) {
                String[] mails = to.split(";");
                addr = new Address[mails.length];
                for (int x = 0; x < mails.length; x++) {
                    Address[] ad = InternetAddress.parse(mails[x]);
                    addr[x] = ad[0];
                }
            } else {
                addr = InternetAddress.parse(to);
            }
        } catch (Exception e) {
            System.out.println("Error: Creating the Address list to send mail. "
                    + "Message: " + e.getMessage());
        }
        return addr;
    }

    /**
     * Gmail method to send an email.
     *
     * @param from who is sending the mail.
     * @param password the password to authenticate.
     * @param to the email addresses to delivery this mail.
     * @param subject the message subject.
     * @param content the full content in HTML.
     */
    public void sendMessage(final String from, final String password,
            String to, String subject, String content) {
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.host", SMTP_HOST);
        props.put("mail.smtp.socketFactory.port", SMTP_PORT);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", SMTP_PORT);

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });
        //If is true, then the log is printed
        session.setDebug(false);
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            Address[] recipients = this.buildRecipients(to);
            message.setRecipients(Message.RecipientType.TO, recipients);
            message.setSubject(subject);
            message.setContent(content, "text/html");
            Transport.send(message);
        } catch (MessagingException e) {
            System.out.println("Error: Fail to send mail. Message: " + e.getMessage());
        }
    }

    public void sendMessageAttach(final String from, final String password,
            String to, String subject, String content, String filePath) {
        Properties props = new Properties();
        props.put("mail.smtp.host", SMTP_HOST);
        props.put("mail.smtp.socketFactory.port", SMTP_PORT);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", SMTP_PORT);
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });
        session.setDebug(false);
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            Address[] recipients = this.buildRecipients(to);
            message.setRecipients(Message.RecipientType.TO, recipients);
            message.setSubject(subject);

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(content, "text/html");

            MimeBodyPart mbp2 = new MimeBodyPart();
            FileDataSource fds = new FileDataSource(filePath);
            mbp2.setDataHandler(new DataHandler(fds));
            mbp2.setFileName(fds.getName());

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);
            mp.addBodyPart(mbp2);
            message.setContent(mp);

            message.setSentDate(new Date());
            Transport.send(message);
        } catch (MessagingException e) {
            System.out.println("Error: Fail to send mail. Message: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Gmail().sendMessage("apollo.unisinos@gmail.com", "Nov@2018", "motaalvesb@gmail.com", "Apollo EFP - Emergência!",
                "Teste pode ter sido vítima de uma queda. "
                        + "O serviço de emergência foi acionado automaticamente. "
                        + "Verifique imediatamente se teste precisa de ajuda.");
    }
}
