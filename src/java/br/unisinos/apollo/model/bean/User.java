package br.unisinos.apollo.model.bean;

import java.util.Date;

/**
 * Created 05/05/2018
 *
 * @author Bruno Mota [brunomota55@gmail.com]
 */
public class User {

    private Integer userID;
    private String name;
    private Boolean fallHistory;
    private Double height;
    private Double weight;
    private String mail;
    private Date bornDate;

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getFallHistory() {
        return fallHistory;
    }

    public void setFallHistory(Boolean fallHistory) {
        this.fallHistory = fallHistory;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Date getBornDate() {
        return bornDate;
    }

    public void setBornDate(Date bornDate) {
        this.bornDate = bornDate;
    }

}
