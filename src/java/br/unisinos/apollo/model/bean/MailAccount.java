package viso.mota.mail.model.bean;

import java.util.Date;

/**
 * The mail configuration bean.
 *
 * Created 09/09/2016 Updated 12/09/2016
 *
 * @author Bruno Mota
 */
public class MailAccount {

    private Integer id;
    private Date logDate;
    private String address;
    private String password;
    private String hash;

    /**
     * @return the id.
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the logDate.
     */
    public Date getLogDate() {
        return logDate;
    }

    /**
     * @param logDate the logDate to set.
     */
    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    /**
     * @return the address.
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the hash.
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set.
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

}
