package br.unisinos.apollo.model.bean;

import java.util.Date;

/**
 * Created 26/05/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
public class MonitoramentoAmbiente {

    private Integer monitoramentoAmbienteID;
    private Integer ambienteID;
    private Double temperatura;
    private Double iluminacao;
    private Date datahora;

    public Integer getMonitoramentoAmbienteID() {
        return monitoramentoAmbienteID;
    }

    public void setMonitoramentoAmbienteID(Integer monitoramentoAmbienteID) {
        this.monitoramentoAmbienteID = monitoramentoAmbienteID;
    }

    public Integer getAmbienteID() {
        return ambienteID;
    }

    public void setAmbienteID(Integer ambienteID) {
        this.ambienteID = ambienteID;
    }

    public Double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Double temperatura) {
        this.temperatura = temperatura;
    }

    public Double getIluminacao() {
        return iluminacao;
    }

    public void setIluminacao(Double iluminacao) {
        this.iluminacao = iluminacao;
    }

    public Date getDatahora() {
        return datahora;
    }

    public void setDatahora(Date datahora) {
        this.datahora = datahora;
    }

    

}
