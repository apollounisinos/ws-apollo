package br.unisinos.apollo.model.bean;

import java.util.Date;

/**
 * Created 10/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
public class Event {

    private Integer eventID;
    private Integer userID;
    private Date datetime;
    private char type;
    private String movUserID;

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public String getMovUserID() {
        return movUserID;
    }

    public void setMovUserID(String movUserID) {
        this.movUserID = movUserID;
    }

}
