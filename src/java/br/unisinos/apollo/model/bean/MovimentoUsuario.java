package br.unisinos.apollo.model.bean;

import java.util.Date;

/**
 * Created 05/05/2018
 *
 * @author Bruno Mota [brunomota55@gmail.com]
 */
public class MovimentoUsuario {

    private Integer monitoramentoUsuarioID;
    private Integer usuarioID;
    private Double moduloAceleracao;
    private Date datahora;
    private Double xInclinacao;
    private Double yInclinacao;
    private Double zInclinacao;
    private Double xAceleracao;
    private Double yAceleracao;
    private Double zAceleracao;

    public Integer getMonitoramentoUsuarioID() {
        return monitoramentoUsuarioID;
    }

    public void setMonitoramentoUsuarioID(Integer monitoramentoUsuarioID) {
        this.monitoramentoUsuarioID = monitoramentoUsuarioID;
    }

    public Integer getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Integer usuarioID) {
        this.usuarioID = usuarioID;
    }

    public Double getModuloAceleracao() {
        return moduloAceleracao;
    }

    public void setModuloAceleracao(Double moduloAceleracao) {
        this.moduloAceleracao = moduloAceleracao;
    }

    public Date getDatahora() {
        return datahora;
    }

    public void setDatahora(Date datahora) {
        this.datahora = datahora;
    }

    public Double getxInclinacao() {
        return xInclinacao;
    }

    public void setxInclinacao(Double xInclinacao) {
        this.xInclinacao = xInclinacao;
    }

    public Double getyInclinacao() {
        return yInclinacao;
    }

    public void setyInclinacao(Double yInclinacao) {
        this.yInclinacao = yInclinacao;
    }

    public Double getzInclinacao() {
        return zInclinacao;
    }

    public void setzInclinacao(Double zInclinacao) {
        this.zInclinacao = zInclinacao;
    }

    public Double getxAceleracao() {
        return xAceleracao;
    }

    public void setxAceleracao(Double xAceleracao) {
        this.xAceleracao = xAceleracao;
    }

    public Double getyAceleracao() {
        return yAceleracao;
    }

    public void setyAceleracao(Double yAceleracao) {
        this.yAceleracao = yAceleracao;
    }

    public Double getzAceleracao() {
        return zAceleracao;
    }

    public void setzAceleracao(Double zAceleracao) {
        this.zAceleracao = zAceleracao;
    }

}
