package br.unisinos.apollo.model.dao;

import br.unisinos.apollo.model.bean.Extrinseco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 09/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class ExtrinsecoDAO {

    public Extrinseco getExtrinseco(Connection connection, Integer userID) {
        PreparedStatement pstmt;
        Extrinseco extrinseco = new Extrinseco();
        try {
            String sql = "SELECT * FROM extrinseco WHERE usuario_id = ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, userID);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                extrinseco.setCompromentimentoMobilidade(rs.getBoolean("comprometimentoMobilidade"));
                extrinseco.setRiscoAmbiental(rs.getBoolean("riscoAmbiental"));
                extrinseco.setInternLongaPermanencia(rs.getBoolean("internLongaPermanencia"));
                extrinseco.setViveSozinho(rs.getBoolean("viveSozinho"));
                extrinseco.setReducaoAtividadesBasicas(rs.getBoolean("reducaoAtividadesBasicas"));
                extrinseco.setCapacidadeManterseEmPe(rs.getBoolean("capacidadeManterseEmPe"));
                extrinseco.setCapacidadeTransferirse(rs.getBoolean("capacidadeTransferirse"));
                extrinseco.setCapacidadeDeitarLevantar(rs.getBoolean("capacidadeDeitarLevantar"));
                extrinseco.setDependenciaFuncional(rs.getBoolean("dependenciaFuncional"));
                extrinseco.setMuitoAtivoFisicamente(rs.getBoolean("muitoAtivoFisicamente"));
                extrinseco.setSedentarismo(rs.getBoolean("sedentarismo"));
                extrinseco.setPolifarmacia(rs.getBoolean("polifarmacia"));
                extrinseco.setUsoDrogasPsicoativas(rs.getBoolean("usoDrogasPsicoativas"));
                extrinseco.setBenzoDiazepinicos(rs.getBoolean("benzoDiazepinicos"));
                extrinseco.setAntiPsicoticos(rs.getBoolean("antiPsicoticos"));
                extrinseco.setAntiDepressivos(rs.getBoolean("antiDepressivos"));
                extrinseco.setSedativos(rs.getBoolean("sedativos"));
                extrinseco.setAnsioliticos(rs.getBoolean("ansioliticos"));
                extrinseco.setHipnoticos(rs.getBoolean("hipnoticos"));
                extrinseco.setFenotiazina(rs.getBoolean("fenotiazina"));
                extrinseco.setAntiHipertensivos(rs.getBoolean("antiHipertensivos"));
                extrinseco.setAntiEpileticos(rs.getBoolean("antiEpileticos"));
                extrinseco.setConsumoAlcool(rs.getBoolean("consumoAlcool"));
            }
        } catch (Exception e) {
            System.out.println("#Error: Return the extrinseco. Message: " + e.getMessage());
        }
        return extrinseco;
    }

}
