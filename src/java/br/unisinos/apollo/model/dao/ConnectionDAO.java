package br.unisinos.apollo.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Connection DAO class used to connect and disconnect JDBC.
 *
 * @author Bruno Mota
 */
public class ConnectionDAO {

    /**
     * Method used to establish a connection with database.
     *
     * @return Connection to perform operations.
     */
    public Connection connect() {
        Connection connection = null;
        try {
            String url = "jdbc:postgresql://localhost:5432/Apollo";
            String usuario = "postgres";
            String senha = "root";
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, usuario, senha);
        } catch (SQLException e) {
            System.out.println("#Error: Fail to connect database. Message: " + e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConnectionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }

    /**
     * Method user to finish the connection with database.
     *
     * @param connection the database connection to close.
     */
    public void disconnect(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("#Error: Fail to disconnect the database. Message: " + e.getMessage());
        }
    }
}
