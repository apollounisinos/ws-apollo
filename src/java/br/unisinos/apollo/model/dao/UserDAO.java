package br.unisinos.apollo.model.dao;

import br.unisinos.apollo.model.bean.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 28/08/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
public class UserDAO {

    public User getUserInfo(Connection connection, Integer userID) {
        PreparedStatement pstmt;
        User user = new User();
        try {
            String sql = "SELECT * FROM usuario WHERE usuario_id = ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, userID);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                user.setUserID(rs.getInt("usuario_id"));
                user.setName(rs.getString("nome"));
                user.setWeight(rs.getDouble("peso"));
                user.setMail(rs.getString("email"));
                user.setHeight(rs.getDouble("altura"));
                user.setBornDate(rs.getDate("datanascimento"));
            }
        } catch (Exception e) {
            System.out.println("#Error: Return the user information. Message: " + e.getMessage());
        }
        return user;
    }

}
