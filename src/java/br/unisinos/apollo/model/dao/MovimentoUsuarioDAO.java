package br.unisinos.apollo.model.dao;

import br.unisinos.apollo.model.bean.MovimentoUsuario;
import br.unisinos.apollo.model.util.DateUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created 05/05/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class MovimentoUsuarioDAO {

    public void movimentoUsuario(Connection connection, MovimentoUsuario monUsuario) {
        PreparedStatement pstmt;
        try {
            String sql = "INSERT INTO movimentousuario(usuario_id, "
                    + "inclinacao_x, inclinacao_y, inclinacao_z, aceleracao_x, "
                    + "aceleracao_y, aceleracao_z, aceleracao, datahora) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, monUsuario.getUsuarioID());
//            pstmt.setDouble(2, monUsuario.getAceleracao());
//            pstmt.setNull(3, 1);
            pstmt.setDouble(2, monUsuario.getxInclinacao());
            pstmt.setDouble(3, monUsuario.getyInclinacao());
            pstmt.setDouble(4, monUsuario.getzInclinacao());
            pstmt.setDouble(5, monUsuario.getxAceleracao());
            pstmt.setDouble(6, monUsuario.getyAceleracao());
            pstmt.setDouble(7, monUsuario.getzAceleracao());
            pstmt.setDouble(8, monUsuario.getModuloAceleracao());
            pstmt.setTimestamp(9, new DateUtil().dateToTimestamp(monUsuario.getDatahora()));
            pstmt.execute();
        } catch (Exception e) {
            System.out.println("#Error: Inserir novo movimento usuário. Mensagem: " + e.getMessage());
        }
    }

    public void setPositiveFeedback(Connection connection, Integer monitID, Boolean feedback) {
        PreparedStatement pstmt;
        try {
            String sql = "UPDATE movimentousuario SET feedback = ? WHERE movimentousuario_id = ?";
            pstmt = connection.prepareStatement(sql);
            pstmt.setBoolean(1, feedback);
            pstmt.setInt(2, monitID);
            pstmt.execute();
        } catch (Exception e) {
            System.out.println("#Error setting a new feedback. Mensagem: " + e.getMessage());
        }
    }

    public ArrayList<MovimentoUsuario> getDataset(Connection connection) {
        PreparedStatement pstmt;
        ArrayList<MovimentoUsuario> data = new ArrayList<>();
        try {
            String sql = "SELECT * FROM movimentousuario;";
            pstmt = connection.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                MovimentoUsuario mov = new MovimentoUsuario();
                mov.setDatahora(rs.getTimestamp("datahora"));
                mov.setModuloAceleracao(rs.getDouble("aceleracao"));
                mov.setxAceleracao(rs.getDouble("aceleracao_x"));
                mov.setyAceleracao(rs.getDouble("aceleracao_y"));
                mov.setzAceleracao(rs.getDouble("aceleracao_z"));
                mov.setxInclinacao(rs.getDouble("inclinacao_x"));
                mov.setyInclinacao(rs.getDouble("inclinacao_y"));
                mov.setzInclinacao(rs.getDouble("inclinacao_z"));
                data.add(mov);
            }
        } catch (Exception e) {
            System.out.println("#Error: Return the mov information. Message: " + e.getMessage());
        }
        return data;
    }

}
