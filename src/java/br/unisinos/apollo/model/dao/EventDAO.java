package br.unisinos.apollo.model.dao;

import br.unisinos.apollo.model.bean.Event;
import br.unisinos.apollo.model.util.DateUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Created 10/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
public class EventDAO {

    public void persistEvent(Connection connection, Event event) {
        PreparedStatement pstmt;
        try {
            String sql = "INSERT INTO evento(usuario_id, datahora, tipo, movimentousuario_id) VALUES (?, ?, ?, ?);";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, event.getUserID());
            pstmt.setTimestamp(2, new DateUtil().dateToTimestamp(event.getDatetime()));
            pstmt.setString(3, event.getType() + "");
            pstmt.setString(4, event.getMovUserID());
            pstmt.execute();
        } catch (Exception e) {
            System.out.println("#Error: Inserir novo evento. Mensagem: " + e.getMessage());
        }
    }

}
