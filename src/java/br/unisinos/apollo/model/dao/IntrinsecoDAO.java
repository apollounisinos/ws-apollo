package br.unisinos.apollo.model.dao;

import br.unisinos.apollo.model.bean.Intrinseco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 09/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class IntrinsecoDAO {

    public Intrinseco getIntrinseco(Connection connection, Integer userID) {
        PreparedStatement pstmt;
        Intrinseco intrinseco = new Intrinseco();
        try {
            String sql = "SELECT * FROM intrinseco WHERE usuario_id = ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, userID);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                intrinseco.setImc(rs.getDouble("imc"));
                intrinseco.setIdade(rs.getInt("idade"));
                intrinseco.setTemporeacao(rs.getDouble("temporeacao"));
                intrinseco.setAntecedenteQueda(rs.getBoolean("antecedenteQueda"));
                intrinseco.setDeficitEquilibrio(rs.getBoolean("deficitEquilibrio"));
                intrinseco.setDeficitVisual(rs.getBoolean("deficitVisual"));
                intrinseco.setDeficitMarcha(rs.getBoolean("deficitMarcha"));
                intrinseco.setDemencia(rs.getBoolean("demencia"));
                intrinseco.setMedoCair(rs.getBoolean("medoCair"));
                intrinseco.setFraquezaMuscular(rs.getBoolean("fraquezaMuscular"));
                intrinseco.setDeficienciaAuditiva(rs.getBoolean("deficienciaAuditiva"));
                intrinseco.setDeficitPonderal(rs.getBoolean("deficitPonderal"));
                intrinseco.setSensibilidade(rs.getBoolean("sensibilidade"));
                intrinseco.setDeficienciaFisica(rs.getBoolean("deficienciaFisica"));
                intrinseco.setOsteoporose(rs.getBoolean("osteoporose"));
                intrinseco.setArtrite(rs.getBoolean("artrite"));
                intrinseco.setDoencaReumatica(rs.getBoolean("doencaReumatica"));
                intrinseco.setSincope(rs.getBoolean("sincope"));
                intrinseco.setLesaoSistemaNervosoCentral(rs.getBoolean("lesaoSistemaNervosoCentral"));
                intrinseco.setProblemasNoPe(rs.getBoolean("problemasNoPe"));
                intrinseco.setDepressao(rs.getBoolean("depressao"));
                intrinseco.setParkinson(rs.getBoolean("parkinson"));
                intrinseco.setVertigem(rs.getBoolean("vertigem"));
                intrinseco.setDiabetes(rs.getBoolean("diabetes"));
                intrinseco.setAlteracaoPostural(rs.getBoolean("alteracaoPostural"));
                intrinseco.setAntecedenteAVC(rs.getBoolean("antecedenteAVC"));
                intrinseco.setIncontinenciaUrinaria(rs.getBoolean("incontinenciaUrinaria"));
                intrinseco.setComorbidade(rs.getBoolean("comorbidade"));
                intrinseco.setAutoPercepcaoEstadoSaude(rs.getBoolean("autoPercepcaoEstadoSaude"));
                intrinseco.setDor(rs.getBoolean("dor"));
            }
        } catch (Exception e) {
            System.out.println("#Error: Return the intrinseco. Message: " + e.getMessage());
        }
        return intrinseco;
    }

}
