package br.unisinos.apollo.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import viso.mota.mail.model.bean.MailAccount;

/**
 * DAO class used to consult VisoNotify settings on database.
 *
 * Created 09/09/2016 Updated 12/09/2016
 *
 * @author Bruno Mota
 */
public class MailAccountDAO {

    private final DateFormat DATE_AND_TIME = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private final DateFormat DATE = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Retrieve a Boolean value if the specified address exists on database.
     *
     * @param connection to perform the operations on database.
     * @param address the email address used to send the message.
     * @return MailAccount containing the address and password.
     */
    public Boolean validateAccountAddress(Connection connection, String address) {
        PreparedStatement pstmt;
        Boolean exists = false;
        try {
            String sql = "SELECT exists(SELECT id FROM conta WHERE endereco LIKE ?);";
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, address);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                exists = rs.getBoolean("exists");
            }
        } catch (Exception e) {
            System.out.println("#Error: Return boolean if address exists. Message: " + e.getMessage());
        }
        return exists;
    }

    /**
     * Retrieve a Boolean value if the specified address and hash exists on
     * database.
     *
     * @param connection to perform the operations on database.
     * @param address the email address used to send the message.
     * @param hash to validate the user on database.
     * @return MailAccount containing the address and password.
     */
    public Boolean validateHashAddress(Connection connection, String address, String hash) {
        PreparedStatement pstmt;
        Boolean exists = false;
        try {
            String sql = "SELECT exists(SELECT id FROM conta WHERE endereco LIKE ? AND hash LIKE ?);";
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, address);
            pstmt.setString(2, hash);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                exists = rs.getBoolean("exists");
            }
        } catch (Exception e) {
            System.out.println("#Error: Return boolean if address and exists. Message: " + e.getMessage());
        }
        return exists;
    }

    /**
     * Retrieve the Mail Account using the address.
     *
     * @param connection to perform the operations on database.
     * @param address the email address used to send the message.
     * @param hash the hash to validate the call.
     * @return MailAccount containing the address and password.
     */
    public MailAccount retrieveMailAccount(Connection connection, String address,
            String hash) {
        PreparedStatement pstmt;
        MailAccount ma = new MailAccount();
        try {
            String sql = "SELECT * FROM conta WHERE endereco LIKE ? AND hash LIKE ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, address);
            pstmt.setString(2, hash);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                ma.setId(rs.getInt("id"));
                ma.setLogDate(DATE.parse(rs.getString("data_registro")));
                ma.setAddress(rs.getString("endereco"));
                ma.setPassword(rs.getString("senha"));
                ma.setHash(rs.getString("hash"));
            }
        } catch (Exception e) {
            System.out.println("#Error: Return value of mail account. Message: " + e.getMessage());
        }
        return ma;
    }

    /**
     * Retrieve the Mail Account using the address. It's a partial method and
     * it's only returned the id and address.
     *
     * @param connection to perform the operations on database.
     * @param address the email address used to send the message.
     * @return MailAccount containing the address and password.
     */
    public MailAccount retrieveMailAccount(Connection connection, String address) {
        PreparedStatement pstmt;
        MailAccount ma = new MailAccount();
        try {
            String sql = "SELECT * FROM conta WHERE endereco LIKE ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, address);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                ma.setId(rs.getInt("id"));
                ma.setAddress(rs.getString("endereco"));
            }
        } catch (Exception e) {
            System.out.println("#Error: Return value of mail account. Message: " + e.getMessage());
        }
        return ma;
    }

    /**
     * DAO method to persist the log of usage on database.
     *
     * @param connection to perform the operation.
     * @param idConta the id of email account.
     * @param description the description containing the log.
     */
    public void persistLog(Connection connection, Integer idConta, String description) {
        Date date = new Date();
        PreparedStatement pstmt;
        try {
            String sql = "INSERT INTO log(id_conta, data_log, descricao) VALUES (?, ?, ?);";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, idConta);
            pstmt.setString(2, DATE_AND_TIME.format(date));
            pstmt.setString(3, description);
            pstmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * DAO method to persist the log of usage on database. This is only used
     * when the user is unknown.
     *
     * @param connection to perform the operation.
     * @param description the description containing the log.
     */
    public void persistLogNoUser(Connection connection, String description) {
        Date date = new Date();
        PreparedStatement pstmt;
        try {
            String sql = "INSERT INTO log(data_log, descricao) VALUES (?, ?);";
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, DATE_AND_TIME.format(date));
            pstmt.setString(2, description);
            pstmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
