package br.unisinos.apollo.model.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created 28/11/2016
 *
 * @author Bruno Mota
 */
public class DateUtil {

    public java.sql.Timestamp dateToTimestamp(java.util.Date date) {
        return new java.sql.Timestamp(date.getTime());
    }

    public java.sql.Date javaDateToSqlDate(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }

    public java.util.Date stringTimestampToJavaDate(String timestamp) {
        //Dec 9, 2016 10:51:55 AM
        java.util.Date date = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("MMM d, yyyy hh:mm:ss aa");
            date = format.parse(timestamp);
        } catch (Exception e) {
            System.out.println("#Error: Fail to parse date. Message: " + e.getMessage());
        }
        return date;
    }

    public java.util.Date stringTimestampToJavaDate2(String timestamp) {
        //Dec 9, 2016 10:51:55 AM
        java.util.Date date = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
            date = format.parse(timestamp);
        } catch (Exception e) {
            System.out.println("#Error: Fail to parse date. Message: " + e.getMessage());
        }
        return date;
    }

    public java.util.Date stringDateToJavaDate(String datee) {
        //Dec 9, 2016 10:51:55 AM
        java.util.Date date = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            date = format.parse(datee);
        } catch (Exception e) {
            System.out.println("#Error: Fail to parse date. Message: " + e.getMessage());
        }
        return date;
    }

    /**
     * Method used to calculate the days inside an interval.
     *
     * @param beginDate the initial date to be calculated.
     * @param endDate the final date to be calculated.
     * @param intervalOfDate the interval of days to end and begin.
     * @return ArrayList containing the Dates on the interval.
     */
    public ArrayList<Date> intervalDateCalc(Date beginDate, Date endDate, Integer intervalOfDate) {
        ArrayList<Date> dates = new ArrayList<>();
        ArrayList<Integer> blocoData = new ArrayList<>();
        Calendar beginCld = Calendar.getInstance();
        beginCld.setTime(beginDate);
        Calendar endCld = Calendar.getInstance();
        endCld.setTime(endDate);
        double difference = endCld.getTimeInMillis() - beginCld.getTimeInMillis();
        int dayTime = 1000 * 60 * 60 * 24;
        int diffDays = (int) Math.round(difference / dayTime);
        while (diffDays != 0) {
            if (diffDays >= intervalOfDate) {
                diffDays -= intervalOfDate;
                blocoData.add(intervalOfDate);
            } else {
                blocoData.add(diffDays);
                diffDays -= diffDays;
            }
        }
        dates.add(beginDate);
        for (Integer qq : blocoData) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(beginDate);
            cal.add(cal.DAY_OF_MONTH, +qq);
            beginDate = cal.getTime();
            dates.add(beginDate);
        }
        return dates;
    }

    public ArrayList<Date> intervalTimeCalc(Date date) {
        ArrayList<Date> dates = new ArrayList<>();
        try {
            SimpleDateFormat withTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            SimpleDateFormat withoutTime = new SimpleDateFormat("dd/MM/yyyy");
            String dtDef = withoutTime.format(date);

            for (int i = 0; i < 24; i++) {
                //Adding to the list
                String hour = i + "";
                if (hour.length() == 1) {
                    hour = "0" + hour;
                }
                dates.add(withTime.parse(dtDef + " " + hour + ":00:00"));
                dates.add(withTime.parse(dtDef + " " + hour + ":59:59"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dates;
    }

}
