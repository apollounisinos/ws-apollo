package br.unisinos.apollo.model.util;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Created 20/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class WorkbookUtil {

    public XSSFWorkbook openWorkbook(String path) {
        try {
            System.out.println(path);
            FileInputStream file = new FileInputStream(new File(path));
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            return workbook;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public XSSFSheet getSheet(XSSFWorkbook workbook, Integer index) {
        try {
            XSSFSheet sheet = workbook.getSheetAt(index);
            return sheet;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public String readString(Row row, Integer position) {
        String finalValue = "";
        Cell cell = row.getCell(position);
        if (cell != null) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            finalValue = cell.getStringCellValue();
        }
        return finalValue;
    }

    public Double readDouble(Row row, Integer position) {
        Double finalValue = 0.0;
        Cell cell = row.getCell(position);
        if (cell != null) {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            finalValue = cell.getNumericCellValue();
        }
        return finalValue;
    }

    public Double readDoubleDefault(Row row, Integer position, Double defaultValue) {
        Double finalValue = null;
        Cell cell = row.getCell(position);
        try {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            finalValue = cell.getNumericCellValue();
        } catch (Exception e) {
            return defaultValue;
        }
        return finalValue;
    }

    public Long readLongDefault(Row row, Integer position, Long defaultValue) {
        Double finalValue = null;
        Cell cell = row.getCell(position);
        try {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            finalValue = cell.getNumericCellValue();
        } catch (Exception e) {
            return defaultValue;
        }
        return finalValue.longValue();
    }

    public Integer readIntDefault(Row row, Integer position, Integer defaultValue) {
        try {
            return readDoubleDefault(row, position, null).intValue();
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public Date readDate(Row row, Integer position) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        Cell cell = row.getCell(position);
        try {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            date = DateUtil.getJavaDate(Double.parseDouble(cell.getStringCellValue()));
        } catch (Exception e) {
            try {
                date = sdf.parse(cell.getStringCellValue());
            } catch (Exception es) {
            }
        }
        return date;
    }

    public String readStringForValidation(Row row, Integer position) {
        String finalValue = "";
        if (row != null) {
            Cell cell = row.getCell(position);
            if (cell != null) {
                cell.setCellType(Cell.CELL_TYPE_STRING);
                finalValue = cell.getStringCellValue();
            }
        } else {
            finalValue = "";
        }
        return finalValue;
    }
}
