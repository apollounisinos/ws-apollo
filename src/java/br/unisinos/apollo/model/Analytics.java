package br.unisinos.apollo.model;

import br.unisinos.apollo.model.bean.Event;
import br.unisinos.apollo.model.bean.MovimentoUsuario;
import br.unisinos.apollo.model.dao.ConnectionDAO;
import br.unisinos.apollo.model.dao.EventDAO;
import br.unisinos.apollo.model.reader.AnaliticsManager;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created 10/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
public class Analytics {

    public void analysis(String path, Integer limiar) {
        Connection connection = new ConnectionDAO().connect();
        //Select all the data
        List<MovimentoUsuario> data = new AnaliticsManager().read(path);
        //Analyse item by item
        for (int i = 0; i < data.size(); i++) {
            if (checkModule(data.get(i), limiar - 4, limiar - 2)) {
                System.out.println("Module found on " + (limiar - 4) + " between " + (limiar - 2) + " " + data.get(i).getMonitoramentoUsuarioID());
                //Retrieve the interval
                List<MovimentoUsuario> cutted = retrieveCut(data, i);
                //Calculate the inclination Y media
                Double mediumY = mediumY(cutted);
                //If the medium bigger than 10°, save the data on Evento table
                System.out.println(mediumY);
                if (Math.abs(mediumY) > 25.0) {
                    Event event = new Event();
                    event.setUserID(data.get(i).getUsuarioID());
                    event.setType('1');
                    event.setDatetime(data.get(i).getDatahora());
                    event.setMovUserID(data.get(i).getMonitoramentoUsuarioID() + "");
                    new EventDAO().persistEvent(connection, event);
                    System.out.println("Persisted!");
                    i = i + 200;
                }
            } else if (checkModule(data.get(i), limiar - 2, limiar)) {
                System.out.println("Module found on " + (limiar - 2) + " between " + limiar + " " + data.get(i).getMonitoramentoUsuarioID());
                //Retrieve the interval
                List<MovimentoUsuario> cutted = retrieveCut(data, i);
                //Calculate the inclination Y media
                Double mediumY = mediumY(cutted);
                //If the medium bigger than 20°, save the data on Evento table
                System.out.println(mediumY);
                if (Math.abs(mediumY) > 45.0) {
                    Event event = new Event();
                    event.setUserID(data.get(i).getUsuarioID());
                    event.setType('2');
                    event.setDatetime(data.get(i).getDatahora());
                    event.setMovUserID(data.get(i).getMonitoramentoUsuarioID() + "");
                    new EventDAO().persistEvent(connection, event);
                    System.out.println("Persisted!");
                    i = i + 200;
                }
            } else if (checkModule(data.get(i), limiar, 1000)) {
                System.out.println("Module found on bigger than " + limiar + " " + data.get(i).getMonitoramentoUsuarioID());
                //Retrieve the interval
                List<MovimentoUsuario> cutted = retrieveCut(data, i);
                //Calculate the inclination Y media
                Double mediumY = mediumY(cutted);
                //If the medium bigger than 45°, save the data on Evento table
                System.out.println(mediumY);
                if (Math.abs(mediumY) > 45.0) {
                    Event event = new Event();
                    event.setUserID(data.get(i).getUsuarioID());
                    event.setType('3');
                    event.setDatetime(data.get(i).getDatahora());
                    event.setMovUserID(data.get(i).getMonitoramentoUsuarioID() + "");
                    new EventDAO().persistEvent(connection, event);
                    System.out.println("Persisted!");
                    i = i + 200;
                }
            }
        }
    }

    private boolean checkModule(MovimentoUsuario mov, Integer limiarMin, Integer limiarMax) {
        if (mov.getModuloAceleracao() > limiarMin && mov.getModuloAceleracao() <= limiarMax) {
            return true;
        } else {
            return false;
        }
    }

    private List<MovimentoUsuario> retrieveCut(List<MovimentoUsuario> data, int index) {
        int range = 150;
        ArrayList<MovimentoUsuario> cutted = new ArrayList<>();

        if (range > index && data.size() > (index + range)) {
            cutted.addAll(data.subList(0, index + range));

        } else if (range > index && (data.size() - 1) < (index + range)) {
            cutted.addAll(data.subList(0, (data.size() - 1)));

        } else if (range < index && data.size() > (index + range)) {
            cutted.addAll(data.subList(index - range, index + range));

        } else if (range < index && data.size() < (index + range)) {
            cutted.addAll(data.subList(index - range, (data.size() - 1)));
        }
        return cutted;
    }

    private Double mediumY(List<MovimentoUsuario> data) {
        Double sum = 0.0;
        for (int i = 0; i < data.size(); i++) {
            sum = sum + data.get(i).getyInclinacao();
        }
        return sum / data.size();
    }

    public static void main(String[] args) {
        Analytics analytics = new Analytics();
        analytics.analysis("D:\\Dataset.xlsx", 4);
    }

}
