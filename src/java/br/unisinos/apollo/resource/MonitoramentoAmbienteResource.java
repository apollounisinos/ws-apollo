package br.unisinos.apollo.resource;

import br.unisinos.apollo.controller.MonitoramentoAmbienteController;
import br.unisinos.apollo.model.bean.MonitoramentoAmbiente;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * Created 27/05/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
@Path("/monitoramentoAmbiente")
public class MonitoramentoAmbienteResource {

    private final DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @POST
    @Path("/persist")
    public String teste(@HeaderParam("ambiente_id") Integer ambienteID,
            @HeaderParam("temperatura") String temperatura,
            @HeaderParam("iluminacao") String iluminacao,
            @HeaderParam("datahora") String datahora) throws ParseException {
        MonitoramentoAmbiente monitor = new MonitoramentoAmbiente();
        monitor.setAmbienteID(ambienteID);
        monitor.setTemperatura(Double.parseDouble(temperatura));
        monitor.setIluminacao(Double.parseDouble(iluminacao));
        monitor.setDatahora(format.parse(datahora));
        //Call the controller
        new MonitoramentoAmbienteController().persistMonitoramentoAmbiente(monitor);
        System.out.println("Ambiente persist");
        return "Persisted";
    }

    @GET
    @Path("/prediction")
    public String teste(@HeaderParam("usuario_id") Integer usuarioID,
            @HeaderParam("ambiente_id") Integer ambienteID) throws ParseException {
        return "1";
    }

}
