package br.unisinos.apollo.resource;

import br.unisinos.apollo.controller.MovimentoUsuarioController;
import br.unisinos.apollo.model.bean.MovimentoUsuario;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.text.ParseException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * Created 19/04/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
@Path("/movimentoUsuario")
public class MovimentoUsuarioResource {

    private final Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();

    @POST
    @Path("/persist")
    public void teste(String body) throws ParseException {
        List<MovimentoUsuario> monitors = gson.fromJson(body, new TypeToken<List<MovimentoUsuario>>(){}.getType());
        new MovimentoUsuarioController().persistMonitoramentoUsuario(monitors);
        System.out.println("persisted");
    }

    @GET
    @Path("/prediction")
    public String prediction(@HeaderParam("userID") String userID) {
        int randomNum = ThreadLocalRandom.current().nextInt(0, 4 + 1);
//        return randomNum + "";
        return "1";
    }

    @POST
    @Path("/predictionFeedback")
    public void predictionFeed(@HeaderParam("userID") Integer userID,
            @HeaderParam("predictionID") Integer predictionID,
            @HeaderParam("feedback") Boolean feedback) {
        new MovimentoUsuarioController().doPredictionFeedback(userID, predictionID, feedback);
    }

}
