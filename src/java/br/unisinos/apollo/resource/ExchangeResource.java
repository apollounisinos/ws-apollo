package br.unisinos.apollo.resource;

import br.unisinos.apollo.controller.ExchangeController;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * The resource class to Exchange interface.
 *
 * Created 09/09/2016 Updated 12/09/2016
 *
 * @author Bruno Mota
 */
@Path("/exchange")
public class ExchangeResource {

    @POST
    @Path("/send")
    @Consumes("text/plain;charset=ISO8859_1")
    @Produces("text/plain;charset=ISO8859_1")
    public String send(
            @HeaderParam("from") String from,
            @HeaderParam("to") String to,
            @HeaderParam("subject") String subject,
            @HeaderParam("hash") String hash,
            @HeaderParam("filePath") String filePath,
            String body) {
        if(filePath == null || filePath.equals("")) { //Send message by the simple way
            return new ExchangeController().sendMessage(from, to, subject, hash, body);
        } else { //Send message by MIME type
            try {
                System.out.println("local arquivo: " + filePath);
               // byte[] subjectByte = subject.getBytes("ISO-8859-1");
           //     byte[] fileByte = filePath.getBytes("ISO-8859-1");
             //   byte[] bodyByte = body.getBytes("ISO-8859-1");
                return new ExchangeController().sendMessageAttach(from, to, subject,
                        hash, body, filePath);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
